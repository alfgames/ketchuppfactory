﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KetchupCreator : MonoBehaviour
{
    float timer = 0f;

    GameObject tempGameObject;
    
    public GameObject[] objects;
    
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > .8f)
        {

            if (Random.RandomRange(0,10) % 2 == 0)
            {
                tempGameObject = Instantiate(objects[0], transform.position, objects[0].transform.rotation) as GameObject;
            }
            else 
            {
                tempGameObject = Instantiate(objects[1], transform.position, objects[1].transform.rotation) as GameObject;
            }

            timer = 0;

        }

    }



}
