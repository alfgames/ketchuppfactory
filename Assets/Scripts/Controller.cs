﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{

    Quaternion from,to;
    private Animator animator;
    private bool a;
    public Transform hand;
    public float rotateSpeed;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        from = transform.rotation;
        to = new Quaternion(0f, -0.15f, 0,1f);
      }

    // Update is called once per frame
    void Update()
    {
        //if (a)
        //{
        //    hand.Rotate(0, rotateSpeed * Time.deltaTime, 0);
        //}
        //else
        //{
        //    hand.Rotate(0, -rotateSpeed * Time.deltaTime, 0);
        //}
      
       
        if (Input.GetMouseButton(0))
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, to, rotateSpeed * Time.deltaTime);
         
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, from, rotateSpeed * Time.deltaTime);
        }
       
      

             
    }

    //public void OnPointerPressedDown()
    //{
    //     animator.SetBool("IsPressed", true);
    //   // a = true;

    //}

    //public void OnPointerPressedUp()
    //{
    //     animator.SetBool("IsPressed", false);
    //    //a = false;
    //}


}
